Povijest
========

1.1.0 (2018-08-20)
------------------

* Kreirana funkcija ``capitalize_first`` koja vraća string s velikim prvim slovom
* Prepravljena funkcija ``datum_rijecima``


1.0.0 (2017-10-04)
------------------

* Dodane funkcije ``datetime2str``, ``date2str`` i ``datum_rijecima`` za
  konverziju date i datetime objekata u string. Funkcije vraćaju definirani
  string i ako je ulazni objekt None.
