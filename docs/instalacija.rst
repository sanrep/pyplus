.. |naslov| replace:: Instalacija biblioteke pyplus
.. |datum| replace:: \04. listopada 2017.
.. |organizacijska_jedinica| replace:: Odsjek za centralnu evidenciju i programiranje



.. sectnum::


========
|naslov|
========

Aplikacija pyplus se instalira s HCR-ovog lokalnog PyPA servera. Za njegovu lokaciju obrati se voditelju Odsjeka za centralnu evidenciju i programiranje.

Npr. za lokaciju HCR-ovog lokalnog PyPA servera ``http://butterfly.hcr:9090/pypa``, instaliraj aplikaciju pyplus sljedećom naredbom

.. code-block:: bash

    $ pip install --trusted-host butterfly.hcr --extra-index-url http://butterfly.hcr:9090/pypa/ pyplus

Upotreba paketa pyplus:

.. code-block:: python

    import pyplus
