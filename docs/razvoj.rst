.. |naslov| replace:: Razvoj biblioteke pyplus
.. |datum| replace:: \03. listopada 2017.
.. |organizacijska_jedinica| replace:: Odsjek za centralnu evidenciju i programiranje



.. sectnum::


========
|naslov|
========

Preduvjeti
**********
Preduvjeti za rad na razvoju paketa PyPlus:

* Instaliran python3.7, python3.7-dev i pip3
* Instaliran virtualenv
* Instaliran i konfiguriran virtualenvwrapper
* Instaliran git
* Otvoren GitLab korisnički račun
* Uloga razvojnog inženjera ili administratora na git projektu pyplus na hcrgit serveru

Instalacija programa virtualenvwrapper nije uvjet, ali u daljnjem tekstu podrazumijeva se da je instaliran.

Pojmovi korišteni u ovom dokumentu opisani su u dokumentu *Pojmovnik* Odsjeka za centralnu evidenciju i programiranje.

Referentna dokumentacija za upravljanje kodom je *Standardne operativne procedure sustava za upravljanje verzijama*.


Pretpostavke
============
U ovom dokumentu korištene su slijedeće pretpostavke:

* Varijabla okoline ``PROJECT_HOME`` postavljena je na razvojni direktorij u kojem se nalaze radni repozitoriji git projekata na radnoj stanici (npr. ``~/dev/``).
* Za sve radnje (osim kreiranja virtualne okoline) podrazumijeva se da je prethodno izvršena naredba ``$ workon pyplus``, odnosno da je postavljena virtualna okolina ``pyplus`` te da je aktivni direktorij ishodišni direktorij projekta pyplus.


Kreiranje razvojne okoline
**************************

Kreiraj virtualnu okolinu pyplus
================================

.. code-block:: bash

    $ mkproject -f --python=/usr/bin/python3.7 pyplus
    # Provjeri verziju python-a
    (pyplus){master}$ python --version


Kloniraj glavni repozitorij git projekta pyplus
===============================================

.. code-block:: bash

    $ workon pyplus
    (pyplus)$ git clone git@gitlab.com:hcrmis/pyplus.git .
    (pyplus){master}$


Instaliraj python pakete za razvoj
==================================

.. code-block:: bash

    (pyplus)$ pip install -U -r requirements/dev.txt


Izvršavanje testova i provjera pokrivenosti koda testovima
**********************************************************

Provjera koda
=============

.. code-block:: bash

  (pyplus)$ flake8 .


Izvrši testove
==============

.. code-block:: bash

  (pyplus)$ coverage run -m pytest


Provjeri pokrivenost koda testovima
===================================

U terminal prozoru
------------------

.. code-block:: bash

  (pyplus)$ coverage report -m


U web pregledniku
-----------------

.. code-block:: bash

  (pyplus)$ coverage html

U web pregledniku otvori datoteku ``pyplus/htmlcov/index.html``.


Generiranje nove oznake inačice
*******************************
Nova oznaka inačice određuje se prema dokumentu *Pravilnik za kreiranje oznaka inačica*. Oznaka inačice je formata ``X.Y.Z``.

Nakon generiranja nove oznake inačice, automatski će se napraviti upis promjena u granu.

Generiraj novu oznaku inačice
=============================

Povećaj X
---------

.. code-block:: bash

    (pyplus){verzija-X.Y.Z}$ bumpversion major


Povećaj Y
---------

.. code-block:: bash

    (pyplus){verzija-X.Y.Z}$ bumpversion minor


Povećaj Z
---------

.. code-block:: bash

    (pyplus){verzija-X.Y.Z}$ bumpversion patch


Kreiraj git oznaku inačice
==========================
Nakon što je odobren zahtjev za integraciju grane inačice u granu master, kreiraj git oznaku za novu inačicu:

#. U web pregledniku otvori `hcrgit server <https://gitlab.com/hcrmis>`_.
#. Izaberi git projekt ``pyplus``.
#. Izaberi poveznicu ``Repository``.
#. Izaberi poveznicu ``Tags``.
#. Izaberi gumb ``New tag``.
#. U polje ``Tag name`` upiši oznaku inačice u formatu vX.Y.Z (npr. v1.3.1).
#. U polju ``Create from`` izaberi ``master``.
#. U polje ``Message`` upiši oznaku inačice identičnu polju ``Tag name``.
#. Izaberi gumb ``Create tag``.


Pakiranje i distribucija aplikacije
***********************************
Preporuča se kreiranje obje vrste paketa za distribuciju, dok se za instalaciju preporuča korištenje wheel paketa.

Kreiraj wheel paket za distribuciju
===================================

.. code-block:: bash

    (pyplus)$ python setup.py bdist_wheel

Kreiran paket ``dist/pyplus-X.Y.Z-py3-none-any.whl``


Kreiraj zip paket za distribuciju
=================================

.. code-block:: bash

    (pyplus)$ python setup.py sdist

Kreiran paket ``dist/pyplus-X.Y.Z.tar.gz``.


Distribucija paketa
===================
Datoteka paketa se može postaviti na file server ili jednostavno kopirati gdje je potrebno.



.. header::

  .. class:: header-table

  +---------------------------------+---------------------------+
  |.. class:: header-left           |.. class:: header-right    |
  |                                 |                           |
  | HRVATSKI CENTAR ZA RAZMINIRANJE | |organizacijska_jedinica| |
  +---------------------------------+---------------------------+

.. footer::

  .. class:: footer-table

  +-----------------------+------------------------+
  |.. class:: footer-left |.. class:: footer-right |
  |                       |                        |
  | |naslov| |datum|      | ###Page###/###Total### |
  +-----------------------+------------------------+
