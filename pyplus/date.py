from datetime import date, datetime


def datetime2str(input_date, datetime_format='%d.%m.%Y. %H:%M', default=''):
    """Pretvori datetime objekt u string.

    Ako input_date nije instanca klase date ili datetime (npr. None) onda vrati
    vrijednost parametra default.

    :param input_date: date ili datetime objekt
    :param datetime_format: format stringa koji se koristi za pretvorbu
                            (strftime)
    :param default: vrijednost koja se vraća ako input_date nije date ni
                    datetime objekt
    :return: datum kao string, odnosno vrijednost parametra default
    """
    if isinstance(input_date, (date, datetime)):
        return input_date.strftime(datetime_format)
    return default


def date2str(input_date, date_format='%d.%m.%Y.', default=''):
    """Pretvori date objekt u string.

    Ako input_date nije instanca klase date ili datetime (npr. None) onda vrati
    vrijednost parametra default.

    :param input_date: date ili datetime objekt
    :param date_format: format stringa koji se koristi za pretvorbu (strftime)
    :param default: vrijednost koja se vraća ako input_date nije date ni
                    datetime objekt
    :return: datum kao string, odnosno vrijednost parametra default
    """
    return datetime2str(input_date, datetime_format=date_format, default=default)


def datum_rijecima(datum, godina=True, default=''):
    """Pretvori datum u string gdje je datum prikazan riječima.

    Mjesec se prikazuje riječima, a u slučaju prikaza godine dodaje se i
    riječ godina. Npr. '25. rujna 2017. godine'.

    Ulazni datum može biti instanca klase date ili datetime, ili string u
    formatu `dd.mm.yyyy.`, odnosno `dd.mm.`` ako je godina False.
    U protivnom se vraća vrijednost parametra default.

    Da bi se mjesec prikazao na hrvatskom prema potrebi postaviti `locale`:
    import locale
    locale.setlocale(locale.LC_ALL, ('hr_HR', 'UTF8'))

    :param datum: date, odnosno datetime objekt, ili string
    :param godina: ako je True uključi godinu u vrijednost koja se vraća
    :param default: vrijednost koja se vraća ako datum nije date ni datetime
                    objekt, niti string u prihvatljivom formatu
    :return: datum kao string, odnosno vrijednost parametra default
    """
    if isinstance(datum, str):
        format_datuma = '%d.%m.%Y.' if godina else '%d.%m.'
        try:
            datum = datetime.strptime(datum, format_datuma)
        except ValueError:
            return default
    if not isinstance(datum, date):
        return default

    # provjera zbog moguće starije verzije paketa locale za hrvatski
    if date(2018, 9, 1).strftime('%B') == 'Rujan':
        return datum_rijecima_stara_verzija(datum, godina, default)

    format_datuma = '%d. %B %Y. godine' if godina else '%d. %B'
    return datum.strftime(format_datuma)


def datum_rijecima_stara_verzija(datum, godina=True, default=''):
    """Pretvori date objekt u string gdje je datum opisan riječima. Stara verzija.

    Ovo je stara verzija funkcije koja postoji, zbog verzije locale za
    hrvatski koja se koristi u starijim distribucijama (npr. stretch).

    U datumu se mjesec prikazuje riječima, te se u slučaju prikaza godine dodaje
    riječ godina. Npr. '25. rujna 2017. godine'.

    Ako datum nije instanca klase date ili datetime onda vrati vrijednost
    parametra default.

    :param datum: date ili datetime objekt
    :param godina: ako je True uključi godinu u vrijednost koja se vraća
    :param default: vrijednost koja se vraća ako datum nije date ni datetime
                    objekt
    :return: datum kao string, odnosno vrijednost parametra default
    """
    if not isinstance(datum, date):
        return default
    mjesec = [
        'siječnja',
        'veljače',
        'ožujka',
        'travnja',
        'svibnja',
        'lipnja',
        'srpnja',
        'kolovoza',
        'rujna',
        'listopada',
        'studenog',
        'prosinca',
    ]
    datum_rijecima = datum.strftime('%d. ') + mjesec[datum.month - 1]
    if godina:
        datum_rijecima += datum.strftime(' %Y. godine')
    return datum_rijecima
