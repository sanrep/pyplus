import zlib


def checksum(text):
    return adler32_checksum(text)


def adler32_checksum(text):
    return zlib.adler32(text.encode('utf-8'))


def signed32_checksum(text):
    return unsigned_to_signed32(adler32_checksum(text))


def signed_to_unsigned32(number):
    return number & ((1 << 32) - 1)


def signed_to_unsigned64(number):
    return number & ((1 << 64) - 1)


def unsigned_to_signed32(number):
    return (number & ((1 << 31) - 1)) - (number & (1 << 31))


def unsigned_to_signed64(number):
    return (number & ((1 << 63) - 1)) - (number & (1 << 63))
