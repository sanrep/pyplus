

def capitalize_first(text):
    """Capitalize first letter, leave the rest unchanged."""
    if not text:
        return ''
    if len(text) == 1:
        return text[0].upper()
    return text[0].upper() + text[1:]
