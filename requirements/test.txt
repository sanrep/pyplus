# Testing dependencies
-r base.txt

# tests
pytest==3.7.1
pytest-cov==2.5.1

# test coverage
coverage==4.4.1

# code check
flake8==3.4.1
pep8-naming==0.7.0
