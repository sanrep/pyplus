#!/usr/bin/env python
import os
import re

from setuptools import find_packages, setup


def get_version(*file_paths):
    """Retrieves the version from pyplus/__init__.py"""
    filename = os.path.join(os.path.dirname(__file__), *file_paths)
    version_file = open(filename).read()
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError('Unable to find version string.')


version = get_version("pyplus", "__init__.py")

readme = open('README.rst').read()
history = open('HISTORY.rst').read().replace('.. :changelog:', '')

setup(
    name='pyplus',
    version=version,
    description="""Biblioteka često korištenih funkcija""",
    long_description=readme + '\n\n' + history,
    author='Odsjek za centralnu evidenciju i programiranje',
    author_email='sandi.saban@hcr.hr',
    url='https://gitlab.com/hcrmis/pyplus',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[],
    license="",
    zip_safe=False,
    keywords='pyplus',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: Other/Proprietary License',
        'Natural Language :: Croatian',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ],
)
