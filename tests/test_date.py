from datetime import date, datetime
import locale
import pytest

from pyplus.date import (
    date2str,
    datetime2str,
    datum_rijecima,
    datum_rijecima_stara_verzija,
)


class TestDateFunctions(object):
    @pytest.fixture
    def set_up(self):
        locale.setlocale(locale.LC_ALL, ('hr_HR', 'UTF8'))
        self.vrijeme = datetime(2017, 9, 25, 8, 35)
        self.datum = date(2017, 9, 25)

    def test_datetime2str(self, set_up):
        assert datetime2str(None) == ''
        assert datetime2str(None, default='prazno') == 'prazno'
        assert datetime2str('25.09.2017.') == ''
        assert datetime2str(self.vrijeme) == '25.09.2017. 08:35'
        assert datetime2str(self.vrijeme, datetime_format='%d.%m.') == '25.09.'
        assert datetime2str(self.datum) == '25.09.2017. 00:00'

    def test_date2str(self, set_up):
        assert date2str(None) == ''
        assert date2str(None, default='prazno') == 'prazno'
        assert date2str('25.09.2017.') == ''
        assert date2str(self.datum) == '25.09.2017.'
        assert date2str(self.datum, date_format='%d.%m.') == '25.09.'
        assert date2str(self.vrijeme) == '25.09.2017.'
        assert date2str(self.vrijeme, date_format='%d.%m. %H:%M') == '25.09. 08:35'

    def test_datum_rijecima(self, set_up):
        assert datum_rijecima(None) == ''
        assert datum_rijecima(None, default='prazno') == 'prazno'
        assert datum_rijecima('25.09.2017.') == '25. rujna 2017. godine'
        assert datum_rijecima('25.09.', godina=False) == '25. rujna'
        assert datum_rijecima('25.09.2017.', godina=False) == ''
        assert datum_rijecima(self.datum) == '25. rujna 2017. godine'
        assert datum_rijecima(self.datum, godina=False) == '25. rujna'
        assert datum_rijecima(self.vrijeme) == '25. rujna 2017. godine'

    def test_datum_rijecima_stara_verzija(self, set_up):
        assert datum_rijecima_stara_verzija(None) == ''
        assert datum_rijecima_stara_verzija(None, default='prazno') == 'prazno'
        assert datum_rijecima_stara_verzija('25.09.2017.') == ''
        assert datum_rijecima_stara_verzija(self.datum) == '25. rujna 2017. godine'
        assert datum_rijecima_stara_verzija(self.datum, godina=False) == '25. rujna'
        assert datum_rijecima_stara_verzija(self.vrijeme) == '25. rujna 2017. godine'
