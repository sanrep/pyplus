from pyplus.utils import capitalize_first


class TestUtilsFunctions(object):
    def test_capitalize_first(self):
        assert capitalize_first('') == ''
        assert capitalize_first('a') == 'A'
        assert capitalize_first('č') == 'Č'
        assert capitalize_first('testX') == 'TestX'
